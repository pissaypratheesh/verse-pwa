/*
 * Created by pratheesh.pm on 18/05/17.
 */
import React from 'react';

export default class HeaderOnScroll extends React.Component {
  constructor(props) {
    super(props);
    this.navClick = this.navClick.bind(this);

  }

  navClick(type){
    this.props.selectActive(type);
  }

  componentDidMount() {
  }

  componentWillUnmount() {
  }

  componentDidUpdate() {
  }

  componentWillReceiveProps(nextProps) {
  }

  render() {
    let context = this;
    window.onscroll = function(){
      document.getElementById("header-scl").style.display =
        (window.pageYOffset > 200) ? "block" : "none";
    };

    return (
      <header id="header-scl" style={{"display":"none"}}>
        <nav>
          <ul className="lhs">
            {context.props.nav.map(function (item, itr) {
              return (<li className="rippl" key={itr}>
                <a href="javaScript:void(0);"
                   className={(item === context.props.active) ? "active":""}
                   onClick={()=>context.navClick(item)}>{item}</a>
              </li>)
            })}
          </ul>
          <div className="rhs"><a href="javaScript:void(0);" className="btnMore"></a></div>
        </nav>
      </header>  );
  }
}
