/**
 * Created by pratheesh on 17/5/17.
 */
import React from 'react';

const TrendsListing = ({webpSrc, webpAlt, imgSrc, imgAlt, title, trendTopic}) => {
  return (
    <li>
      <a href="javaScript:void(0);" className="ripple">
        <h3>{title}</h3>
        <picture>
          <source src={webpSrc} />
          <img src={imgSrc} alt={imgAlt} />
        </picture>
      </a>
    </li>

  );
};

export default TrendsListing;
