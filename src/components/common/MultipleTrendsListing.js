/**
 * Created by pratheesh on 17/5/17.
 */
import TrendsListing from './TrendsListing';
import React from 'react';
var _ = require('underscore');
_.mixin(require('../../lib/mixins'));

// multiple trending topic list
const MultipleNewsListing = ({topics}) => {
  return (
    <div  id="topic" className="dataWarp active">
      <ul className="topic-list-item">
        {_.at(topics,'TRENDING') && (_.at(topics,'TRENDING').map(function(item, itr){
            return <TrendsListing webpSrc= {item.url.split(".jpg")[0] + ".webp"}
                                webpAlt=""
                                imgSrc= {item.url}
                                imgAlt=""
                                title={item.title}
                                key = {itr}/>
          }
        ))}
      </ul>
      <div className="featured-item-list">
        <h2>Featured</h2>
        <ul>
          {_.at(topics,'FEATURED') && (_.at(topics,'FEATURED')).map(function(item, itr){
              return <li className="ripple" key={itr}><a href="javaScript:void(0);">{item.title}</a></li>
            })}
        </ul>
      </div>
    </div>
  );
};

export default MultipleNewsListing;

