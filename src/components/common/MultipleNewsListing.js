/**
 * Created by pratheesh on 17/5/17.
 */
import NewsListing from "./NewsListing";
import React from 'react';
var _ = require('underscore');
_.mixin(require('../../lib/mixins'));

const MultipleNewsListing = ({news}) => {
  return (
    <div id="headlines" className="dataWarp active">
      <ul className="dh-list-item">
        {_.at(news,'rows') && (news.rows.map(function(item, itr){
            return <NewsListing webpSrc= {item.url.split(".jpg")[0] + ".webp"}
                                 webpAlt=""
                                 imgSrc= {item.url}
                                 imgAlt=""
                                 title={item.title}
                                 npName={item.sourceNameEn}
                                 key = {itr}
                                 time={item.publishTime}/>
          }
        ))}
      </ul>
    </div>
  );
};

export default MultipleNewsListing;



