/**
 * Created by pratheesh on 17/5/17.
 */
import React from 'react';

const NewsListing = ({webpSrc, webpAlt, imgSrc, imgAlt, title, npName, time}) => {
  return (
    <li className="ripple">
      <picture>
        <source srcSet={webpSrc} alt={webpAlt} type="image/webp" />
        <img src={imgSrc} alt={imgAlt} />
      </picture>
      <div className="list-content">
        <h2>{title}</h2>
        <div className="srcInfo">
          <span>{npName}</span>
          <em>{time}</em>
        </div>
      </div>
    </li>
  );
};


export default NewsListing;
