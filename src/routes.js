/**
 * Created by m01352 on 06/04/17.
 */
import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './pages/App';
import HomePage from './pages/HomePage';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={HomePage}/>
  </Route>
);

/* usage:
 <Route path='/' component={Master}>
 <IndexRedirect to='/admin'/>
 <Route path={'/'}>
   <Route path='admin/createNs' component={CreateNs} />
   <Route path='admin/deleteNs' component={DeleteNs}/>
   <Route path='admin/createUser' component={CreateUser}/>
   <Route path='admin/masterConfig' component={MasterConfig}/>
   <Route path='admin/:ns' component={Switch}/>
   <Route path='admin/:ns/history' component={AuditHistory}/>
   <Route path='admin/:ns/settings' component={SettingsNs}/>
 </Route>
 <Route path='*' component={Default}/>
 </Route>
*/
