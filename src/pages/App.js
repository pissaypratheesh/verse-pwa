// This component handles the App template used on every page.
import React, {PropTypes} from 'react';

import '../styles/main.css';


class App extends React.Component {
  render() {
    return (
      <div>
        {this.props.children}
      </div>
    );
  }
}

export default App;
