import React from 'react';
import MultipleNewsListing from "../components/common/MultipleNewsListing";
import MultipleTrendsListing from "../components/common/MultipleTrendsListing";
import Header from '../components/common/Header';
import HeaderOnScroll from '../components/common/HeaderOnScroll';
import Hammer from 'react-hammerjs';
import { post, get } from 'axios';
var _ = require('underscore');
_.mixin(require('../lib/mixins'));

class HomePage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      active:"headlines",
      nav:["headlines", "topics", "newspaper"]
    };
    this.handleSwipe = this.handleSwipe.bind(this);
    this.onTap = this.onTap.bind(this);
    this.selectActive = this.selectActive.bind(this);
  }

  selectActive(active){
    console.log(" wooow-->",active);
    this.setState({
      active:active
    })
  }

  componentDidMount(){
    let context = this;
    get('/apis/news').then(function (resp) {
      context.setState({
        news: _.at(resp,'data.data')
      })
    })
    get('/apis/topics').then(function (resp) {
      context.setState({
        topics: _.at(resp,'data')
      })
    }).catch(function (err) {
      console.log("errored-->",err)
    })

  }

  handleSwipe(e){
    let direction = (e.direction === 2) ? 1 : ((e.direction === 4) ? -1 : 0);
    console.log("\n swipe params-->",e.direction,direction,typeof direction);

    this.setState({
      active: this.state.nav[Math.abs((_.indexOf(this.state.nav,this.state.active) + direction)) % this.state.nav.length]
    })
    // get('/apis/news')
    //   .then(function (response) {
    //     console.log(response);
    //   })
    //   .catch(function (error) {
    //     console.log(error);
    //   });

  }

  onTap(a,b,c,d){
    console.log("\n tap params-->",this.state.nav[_.find(this.state.nav,this.state.active) + 1]);

    get('/apis/news')
      .then(function (response) {
        console.log("\n\n pratheeesh-->",response);
      })
      .catch(function (error) {
        console.log( "Error-->",error);
      });

  }

  render() {
    let context = this;
    console.log("\n\n in render of home-->",this.state)
    return (

      <div>

        <Header active={context.state.active} nav={context.state.nav} selectActive={context.selectActive}/>
        <HeaderOnScroll active={context.state.active} nav={context.state.nav} selectActive={context.selectActive}/>

        <Hammer onSwipe={this.handleSwipe} onTap={this.onTap} direction="DIRECTION_HORIZONTAL">
          <div>
            {(context.state.active === "headlines") && <MultipleNewsListing news={this.state.news}/>}
            {(context.state.active === "topics") && <MultipleTrendsListing topics={this.state.topics}/>}
            {(context.state.active === "newspaper") && <MultipleNewsListing news={this.state.news}/>}
          </div>
        </Hammer>

      </div>
    );
  }
}

export default HomePage;
