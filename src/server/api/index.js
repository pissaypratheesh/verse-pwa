/**
 * Created by pratheesh on 19/5/17.
 */
'use strict';

var express = require('express');
var router = express.Router()
var urlMap = require('../../config/urls.js');
var moment = require('moment');
var _ = require('underscore');
import { post, get } from 'axios'

_.mixin(require('../../lib/mixins'));


router.get('/news', function(req, res) {
   get(urlMap.news).then(function (resp) {
    var data = resp && resp.data && resp.data.data;//_.at(resp,'data.data');
    if(data){
      var finalResp = {
        code: 200,
        data: {
          count : data.count,
          nextPageUrl : data.nextPageUrl,
          rows: _.map(data.rows,function (row) {
            var rowData =  _.pick(row,'title','sourceNameEn');
            rowData.url = row && row.contentImage && row.contentImage.url;//_.at(row,'contentImage.url') || '';
            rowData.publishTime = moment(row.publishTime).fromNow();
            return rowData;
          })
        }
      }
      return res.status(200).json(finalResp);
    }
  }).catch(function (error) {
    console.log("Server error:",error);
    return res.status(500).send({error:"server error"});
  });
  return;
});

router.get('/topics', function(req, res) {
  get(urlMap.topics).then(function (resp) {
  //  var data = _.at(resp,'data.data.sections');
    var data = resp && resp.data && resp.data.data && resp.data.data.sections;
    var respData = {};
    data.map(function(itm, itr){
      respData[itm.type] = _.map(itm.kids.rows,function(details){
        return {
          title:details.languageNameMapping.en,
          url: details.bannerImageUrl
        };
      })
    })
    return res.status(200).json(respData);
  }).catch(function (error) {
    console.log("Server error:",error);
    return res.status(500).send({error:"server error"});
  });
  return;
});


module.exports = router;

