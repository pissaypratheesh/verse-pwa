/* eslint-disable no-console */
/*eslint-disable import/default */
import React from 'react';
import { render } from 'react-dom';
import ReactDOM from 'react-dom';
import { Router, Route, browserHistory } from 'react-router';
import routes from './routes';
//import './styles/styles.css'; //Webpack can import CSS files too!


(function () {

  ReactDOM.render(
    (
      <Router history={browserHistory} routes={routes} />
    )
    , document.getElementById('app'));

})();
